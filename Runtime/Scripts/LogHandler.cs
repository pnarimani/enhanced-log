﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.Scripting;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

[assembly: AlwaysLinkAssembly]

namespace EnhancedLog
{
    internal class LogHandler
    {
        private static UdpClient udpClient;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Initialize()
        {
            udpClient = new UdpClient();
            Application.logMessageReceived += ApplicationOnLogMessageReceived;
        }

        private static void ApplicationOnLogMessageReceived(string condition, string stacktrace, LogType type)
        {
            EnhancedLogSettings settings = EnhancedLogSettings.Instance;

            if (settings == null)
                return;

            if (settings.LogTargets == null)
                return;

            if (settings.LogTargets.Count == 0)
                return;

            LogLevel level = LogLevel.Trace;

            if (condition.StartsWith("[Trace]"))
                level = LogLevel.Trace;
            else if (condition.StartsWith("[Debug]"))
                level = LogLevel.Info;
            else if (condition.StartsWith("[Warning]"))
                level = LogLevel.Warning;
            else if (condition.StartsWith("[Error]"))
                level = LogLevel.Error;
            else if (condition.StartsWith("[Fatal]"))
                level = LogLevel.Fatal;
            else if(!settings.SendDefaultUnityLogs)
                return;
            
            byte[] bytes = Encoding.UTF8.GetBytes(condition + "\n" + stacktrace);

            foreach (LogTarget target in settings.LogTargets)
            {
                if (target.MinimumLog > level)
                    continue;

                switch (target.Type)
                {
                    case LogTarget.TargetType.Remote:
                        udpClient.SendAsync(bytes, bytes.Length, target.TargetIP, target.TargetPort);
                        break;
                    case LogTarget.TargetType.File:
                        string filePath = Path.Combine(Application.persistentDataPath, "Logs", target.LogFileName);
                        using (FileStream stream = new FileStream(filePath, FileMode.Append, FileAccess.Write))
                            stream.WriteAsync(bytes, 0, bytes.Length);
                        break;
                }
            }
        }
        
        [DebuggerHidden]
        [DebuggerNonUserCode]
        public static void HandleLog(string log, Object context, LogLevel level)
        {
            EnhancedLogSettings settings = EnhancedLogSettings.Instance;

            if (settings == null)
                return;

            if (level < settings.UnityConsoleMinLogLevel)
                return;
            
            switch (level)
            {
                case LogLevel.Trace:
                    Debug.Log(log, context);
                    break;
                case LogLevel.Info:
                    Debug.Log(log, context);
                    break;
                case LogLevel.Warning:
                    Debug.LogWarning(log, context);
                    break;
                case LogLevel.Error:
                    Debug.LogError(log, context);
                    break;
                case LogLevel.Fatal:
                    Debug.LogError(log, context);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}