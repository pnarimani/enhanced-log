using System;
using UnityEngine;

namespace EnhancedLog
{
    [Serializable]
    public class LogTarget
    {
        public enum TargetType
        {
            Remote,
            File
        }

        [SerializeField] private TargetType type;
        [SerializeField] private LogLevel minimumLog;
        [SerializeField] private string targetIP;
        [SerializeField] private int targetPort;
        [SerializeField] private string logFileName;

        public LogLevel MinimumLog => minimumLog;

        public string TargetIP => targetIP;

        public string LogFileName => logFileName;

        public TargetType Type => type;

        public int TargetPort => targetPort;
    }
}