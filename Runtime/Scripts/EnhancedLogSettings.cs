﻿using System.Collections.Generic;
using UnityEngine;

namespace EnhancedLog
{
    public class EnhancedLogSettings : ScriptableObject
    {
        private static EnhancedLogSettings instance;

        [SerializeField] private LogLevel unityConsoleMinLogLevel;
        [SerializeField] private bool sendDefaultUnityLogs = true;
        [SerializeField] private List<LogTarget> logTargets;

        public static EnhancedLogSettings Instance
        {
            get
            {
                if (instance != null)
                    return instance;

                instance = Resources.Load<EnhancedLogSettings>("EnhancedLogSettings");

                if (instance == null)
                    instance = CreateInstance();

                return instance;
            }
        }

        public List<LogTarget> LogTargets => logTargets;

        public LogLevel UnityConsoleMinLogLevel => unityConsoleMinLogLevel;

        public bool SendDefaultUnityLogs => sendDefaultUnityLogs;

        private static EnhancedLogSettings CreateInstance()
        {
            EnhancedLogSettings newSettings = CreateInstance<EnhancedLogSettings>();
            
#if UNITY_EDITOR

            if (!UnityEditor.AssetDatabase.IsValidFolder("Assets/Resources"))
                UnityEditor.AssetDatabase.CreateFolder("Assets", "Resources");

            UnityEditor.AssetDatabase.CreateAsset(newSettings, "Assets/Resources/EnhancedLogSettings.asset");
            UnityEditor.AssetDatabase.SaveAssets();

            UnityEditor.Selection.activeObject = newSettings;

            Debug.LogWarning("No EnhancedLogSettings asset found! Creating new one, ensure it's locatable by Resources",
                newSettings);
#endif
            return newSettings;
        }
    }
}