namespace EnhancedLog
{
    public enum LogLevel
    {
        Trace,
        Info,
        Warning,
        Error,
        Fatal
    }
}